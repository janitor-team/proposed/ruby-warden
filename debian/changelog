ruby-warden (1.2.8-2) UNRELEASED; urgency=medium

  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 11 Sep 2021 12:16:51 -0000

ruby-warden (1.2.8-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ Utkarsh Gupta ]
  * New upstream version 1.2.8
  * Add salsa-ci.yml
  * Switch d/watch to point to g/h tarball
  * Fix package wrt cme
  * Add patch to temporarily skip test
  * Update d/*.docs
  * Rename History.rdoc to CHANGELOG.md

 -- Utkarsh Gupta <utkarsh@debian.org>  Mon, 30 Mar 2020 01:15:08 +0530

ruby-warden (1.2.3-2) unstable; urgency=medium

  * Team upload.
  * Add rspec3-port.patch (Closes: #795123)
    - Ports the tests to support RSpec3 syntax
  * Bump Standards Version to 3.9.6 (no changes)

 -- Balasankar C <balasankarc@autistici.org>  Wed, 02 Sep 2015 12:18:15 +0530

ruby-warden (1.2.3-1) unstable; urgency=low

  * New upstream release.
  * Bump standards version to 3.9.5 (no changes)
  * Bump gem2deb build dep to >= 0.7.5~ (ruby 2.x)
  * Remove explicit_require_rspec.patch

 -- Pirate Praveen <praveen@debian.org>  Sun, 13 Apr 2014 11:39:43 +0530

ruby-warden (1.2.1-2) unstable; urgency=low

  * Upload to unstable
  * Install README.textfile as doc
  * Install History.rdoc as changelog
  * Use debian/ruby-tests.rake for tests
  * Add rake to build-depends

 -- Praveen Arimbrathodiyil <praveen@debian.org>  Wed, 14 Nov 2012 00:20:36 +0530

ruby-warden (1.2.1-1) experimental; urgency=low

  * Initial release (Closes: #624635)

 -- Praveen Arimbrathodiyil <praveen@debian.org>  Thu, 18 Oct 2012 15:59:06 +0530
